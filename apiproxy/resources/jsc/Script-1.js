var phoneNumber = context.getVariable("request.queryparam.phoneNumber");
var timeStamp = new Date().toISOString().replace('Z', '');
context.setVariable("timeStamp", timeStamp);
var phoneRegEx = /^\d{10,}/;

if(phoneNumber === null) {
    context.setVariable("errorMessage", "Please provide the phoneNumber.");
    context.setVariable("path", "/properties/note/properties/phoneNumber/nonExisting");
    context.setVariable("isPhoneValid", false);
} else if (phoneNumber.length < 10) {
    context.setVariable("errorMessage", "String is too short (" + phoneNumber.length + " chars), minimum 10.");
    context.setVariable("path", "/properties/note/properties/phoneNumberbil/minLength");
    context.setVariable("isPhoneValid", false);
} else if(!phoneRegEx.test(phoneNumber)) {
    context.setVariable("errorMessage", "Please provide a 10+ digit phoneNumber, no spaces.");
    context.setVariable("path", "/properties/note/properties/phoneNumber/invalidValue");
    context.setVariable("isPhoneValid", false);
}